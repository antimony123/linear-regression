from linreg import *
from datasets import *
import matplotlib.pyplot as mpp


# Example data set
# ex_mad = LpProblem("Maximum Absolute Deviation Example", LpMinimize)
# ex_sad = LpProblem("Sum of Absolute Deviations Example", LpMinimize)
#     
# min_maxabsdev(small_set, ex_mad)
# min_sumabsdevs(small_set, ex_sad)
#     
# print_lp(ex_mad)
# print_constraints(ex_mad)
# print
# print_lp(ex_sad)
# print_constraints(ex_sad)
    
# bfl1, sc1 = graph_dslp(small_set, 5, 12, ex_mad, "red", "Max Abs. Dev.")
# bfl2, sc2 = graph_dslp(small_set, 5, 12, ex_sad, "green", "Sum of Abs. Devs.")


# Project data set
maxabsdev = LpProblem("Maximum Absolute Deviation Project", LpMinimize)
sumabsdev = LpProblem("Sum of Absolute Deviations Project", LpMinimize)
    
min_maxabsdev(proj_set, maxabsdev)
min_sumabsdevs(proj_set, sumabsdev)

# methods to graph the feasible region, pull them out of main eventually
#
# def pos_c(a, x, y, b):
#     return y - a*x - b
#  
# def neg_c(a, x, y, b):
#     return a*x + b - y
#  
# a = np.arange(2.75, 4, 0.2)
#  
# for x,y in proj_set:
#     mpp.plot(a, pos_c(a, x, y, value(maxabsdev.variablesDict().get('b'))), 'b-')
#  
# for x,y in proj_set:
#     mpp.plot(a, neg_c(a, x, y, value(maxabsdev.variablesDict().get('b'))), 'b-')
# # 
# mpp.plot([value(maxabsdev.variablesDict().get('a'))], [value(maxabsdev.objective)], 'ro')
# test to see if the (a, w) point is on the boundary of the feasible region.
    
# print_lp(maxabsdev)
# print_constraints(maxabsdev)
# print
# print_lp(sumabsdev)
# print_constraints(sumabsdev)
# #

bfl1, sc1 = graph_dslp(proj_set, 14, 50, maxabsdev, "red", "Max Abs. Dev.")
bfl2, sc2 = graph_dslp(proj_set, 14, 50, sumabsdev, "green", "Sum of Abs. Devs.")
  

# mpp.legend()
mpp.show()
