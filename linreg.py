from pulp import *
import numpy as np
import matplotlib.pyplot as mpp

def min_maxabsdev(ds, lp):
    # variable setup
    w = LpVariable('w', 0) # only w is restricted to 0, because it must be positive (absolute value)
    a = LpVariable('a') # a and b can be negative or positive, so they are unrestricted.
    b = LpVariable('b')
    
    # constraints setup
    i = 0
    for x,y in ds:
        lp += (w >= y - (a*x + b))
        lp += (w >= (a*x + b) - y)
        i += 1
        
    # objective setup
    lp += w
    
    lp.solve()

def min_sumabsdevs(ds, lp):
    # variable setup
    w_vars = [LpVariable('w'+str(i),0) for i in range(len(ds))] # same restrictions apply
    sw = sum(w_vars)
    a = LpVariable('a')
    b = LpVariable('b');
    
    # objective setup
    lp += sw
    
    # constraints setup
    i = 0
    for x,y in ds:
        lp += (w_vars[i] >= y - (a*x + b))
        lp += (w_vars[i] >= (a*x + b) - y)
        i += 1

    lp.solve()
    
def print_lp(lp):
    status = lp.status
    print "status: ", LpStatus[status]
    print "values: "
    vd = lp.variablesDict()
    
    for var in sorted(vd.keys()):
        print "\t"+var+": ", value(vd.get(var)) 
    
    print "\tobjective value: ", value(lp.objective)
    

def print_constraints(lp):
    for c in lp.constraints:
        print lp.constraints.get(c)
    
    
def graph_dslp(ds, xlen, ylen, lp, col, l):
    vd = lp.variablesDict()
    
    # best fit line
    x = np.linspace(0, xlen, ylen)
    y = value(vd.get('a'))*x + value(vd.get('b'))
    bfl = mpp.plot(x, y, color=col, label=l)
    
    # scatter plot of original data set
    data = np.array(ds)
    scatter = mpp.scatter(data[:,0], data[:,1], color="blue")
    return bfl, scatter