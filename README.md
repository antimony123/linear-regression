# linear-regression

Project for Math 177 to find linear regressions of data sets using two linear programming methods: sum of absolute deviations and maximum absolute deviation.

Requires PuLP 1.6.9, matplotlib 2.0.2, numpy 1.14.3. Works with python 2.7.
