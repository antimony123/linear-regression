import matplotlib.pyplot as plt
import numpy as np
from datasets import proj_set

def pos_c(a, x, y, b):
    return y - a*x - b

def neg_c(a, x, y, b):
    return a*x + b - y

a = np.arange(2.75, 4, 0.2)

for x,y in proj_set:
    plt.plot(a, pos_c(a, x, y, 0), 'b-')

for x,y in proj_set:
    plt.plot(a, neg_c(a, x, y, 0), 'b-')
    
plt.show()